[![pipeline status](https://gitlab.com/Tritonar/api_flask_token/badges/master/pipeline.svg)](https://gitlab.com/Tritonar/api_flask_token/-/commits/master)
[![coverage report](https://gitlab.com/Tritonar/api_flask_token/badges/master/coverage.svg)](https://gitlab.com/Tritonar/api_flask_token/-/commits/master)

# Token jwt en python

El código verifica si es un token valido (Signature and Payload). El token no es proporcionado por este servicio, solo comparte la _secret_key_ del servicio que lo genero. Retorna un Json con los datos del client y el estado de la verificación  (true-false).


  - [x] El token proporcionado no cuenta con la codificacion correcto.
>HEADER| |
>--|--
>_KEY_|_VALUE_|
>x-access-token|TOKEN_ARRAY
>>```html
>>{
>>    "email": "",
>>    "exp": "",
>>    "iat": "",
>>    "verify": false
>>}
>>```
---
- [x] Si el token tiene el formato correcto y expiro.
>HEADER| |
>--|--
>_KEY_|_VALUE_|
>x-access-token|eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXIxQGV4YW1wbGUuY29tIiwiaWF0IjoxNjA3MTExNzU4LCJleHAiOjE2MDcxMTUzNTh9.Xbi-IQAvK4grvXvS9iLYZ_9TYwVckNQbY8Xo4wkZ22Y
>
```html
{     "email": "user1@example.com",     "exp": "2020-12-04 02:23:56",     "iat": "2020-12-04 02:23:56",     "verify": false
}
```


---
- [x] Si el token tiene el formato correcto y es valido.
>HEADER| |
>--|--
>_KEY_|_VALUE_|
>x-access-token|eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXIxQGV4YW1wbGUuY29tIiwiaWF0IjoxNjA3MTExNzU4LCJleHAiOjE2MDcxMTUzNTh9.Xbi-IQAvK4grvXvS9iLYZ_9TYwVckNQbY8Xo4wkZ22Y
>
```html
{     "email": "user1@example.com",     "exp": "2020-12-04 02:23:56",     "iat": "2020-12-04 02:23:56",     "verify": true
}
```

