import jwt
from datetime import *

def get_payload_verify(token_jwt):
    # secret key use for encode jwt_token
    secret_key="programacion_distribuida_2_delivery"     
    try:
        decoded = jwt.decode(token_jwt, secret_key, algorithms='HS256')
        #  return Json(payload), verify True    
        return {'email': decoded['email'], 'iat': str(datetime.fromtimestamp(decoded['iat'])),
                    'exp': str(datetime.fromtimestamp(decoded['exp'])),'verify':True}
    except (jwt.ExpiredSignature,jwt.InvalidSignatureError,jwt.DecodeError) as e:
        return get_payload(token_jwt)

def get_payload(token_jwt):
    # Se trata de exatraer el payload si es posible
    try:
        print("Invalid Token")
        # Sin la verificacion no se puede determinar si el payload  fue alterado
        decoded = jwt.decode(token_jwt,verify=False)         
        #  return Json(payload), verify False
        return {'email': decoded['email'], 'iat': str(datetime.fromtimestamp(decoded['iat'])), 
                    'exp': str(datetime.fromtimestamp(decoded['exp'])),'verify':False}
    except (jwt.DecodeError, KeyError) as e:
        # Payload bad encoded, verify False.return Json()
        return {'email':'','iat':'','exp':'','verify':False}
