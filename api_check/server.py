from flask import Flask, request, jsonify
from token_verify import *

def create_app():
    app = Flask(__name__)
    #db.init_app(app)

    @app.route('/')
    def index():
        headers = request.headers
        auth_token = headers.get("x-access-token")
        #verifica token
        verify_token = get_payload_verify(auth_token)
        if verify_token['verify']:
            # Json payload user , status code
            print(str(verify_token))
            return jsonify(verify_token),200
        else:
            # Json payload user , status code
            print(str(verify_token))
            return jsonify(verify_token) , 401
    return app


if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0')
