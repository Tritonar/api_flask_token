import pytest , jwt
from datetime import *
# First party modules
from server import create_app


@pytest.fixture
def client():
    app = create_app()
    app.config["TESTING"] = True
    with app.test_client() as client:
        yield client

def test_token_valid(client):
    iat=int(round(datetime.now().timestamp()))
    exp=int(round(datetime.now().timestamp()+3600))
    secret_key="programacion_distribuida_2_delivery"
    payload= {"email": "user1@example.com","iat": iat,"exp": exp}
    token=jwt.encode(payload,secret_key, algorithm='HS256')
    headers = { 'x-access-token': token}
    response = client.get('/', headers=headers)
    assert response.status_code == 200
    assert response.content_type == 'application/json'
    assert isinstance(response.json, dict)
    assert set(response.json.keys()) >= {'email', 'iat', 'exp', 'verify'}
    assert response.json['verify'] == True

def test_token_invalid(client):
    iat=int(round(datetime.now().timestamp()))
    exp=int(round(datetime.now().timestamp()+3600))
    secret_key="programacion_distribuida_2_Falsa_key"
    payload= {"email": "user1@example.com","iat": iat,"exp": exp}
    token=jwt.encode(payload,secret_key, algorithm='HS256')
    headers = { 'x-access-token': token}
    response = client.get('/', headers=headers)
    assert response.status_code == 401
    assert response.content_type == 'application/json'
    assert isinstance(response.json, dict)
    assert set(response.json.keys()) >= {'email', 'iat', 'exp', 'verify'}
    assert response.json['verify'] == False

def test_token_Exp(client):
    iat=int(round(datetime.now().timestamp()-7200))
    exp=int(round(datetime.now().timestamp()-3600))
    secret_key="programacion_distribuida_2_delivery"
    payload= {"email": "user1@example.com","iat": iat,"exp": exp}
    token=jwt.encode(payload,secret_key, algorithm='HS256')
    headers = { 'x-access-token': token}
    response = client.get('/', headers=headers)
    assert response.status_code == 401
    assert response.content_type == 'application/json'
    assert isinstance(response.json, dict)
    assert set(response.json.keys()) >= {'email', 'iat', 'exp', 'verify'}
    assert response.json['verify'] == False

def test_json_imcopleto(client):
    iat=int(round(datetime.now().timestamp()-7200))
    exp=int(round(datetime.now().timestamp()-3600))
    secret_key="programacion_distribuida_2_delivery"
    payload= {"email": "user1@example.com","exp": exp}
    token=jwt.encode(payload,secret_key, algorithm='HS256')
    headers = { 'x-access-token': token}
    response = client.get('/', headers=headers)
    assert response.status_code == 401
    assert response.content_type == 'application/json'
    assert isinstance(response.json, dict)
    assert set(response.json.keys()) >= {'email', 'iat', 'exp', 'verify'}
    assert response.json['verify'] == False
